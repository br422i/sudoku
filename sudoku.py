#!/usr/bin/env python
import csv


def pretty(board):

    print("-"*18)
    for i in range(9):
        s=""
        for j in range(9):
            if board[i][j]==0:
                s+=" "
            else:
                s+=str(board[i][j])
            if (j+1)%3==0:
                s+="|"
            else:
                s+=" "
        print(s)
        if (i+1)%3==0:
            print("-"*18)
        else:
            print(" "*18)

    # for r in board:
        # print(f"{r}")
    # print("==" * 18)


def readboard():
    board = [[0] * 9 for i in range(9)]
    with open("board.csv", newline="") as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        i = 0
        for row in reader:
            if row:
                for j in range(9):
                    if row[j].isdigit():
                        board[i][j] = int(row[j])
            i += 1

    return board


def nextSlot(board):
    for i in range(9):
        for j in range(9):
            if board[i][j] == 0:
                return (i, j)
    return None


def isValidBoard(board):
    m = set()
    for i in range(9):
        for j in range(9):
            if board[i][j] != 0:
                v = board[i][j]
                boxi = i // 3
                boxj = j // 3
                keys = [f"r{i}|{v}", f"c{j}|{v}", f"b{boxi}{boxj}|{v}"]

                for k in keys:
                    if k in m:
                        return False
                    else:
                        m.add(k)

    return True


def solve(board):
    slot = nextSlot(board)
    if not slot:
        return True
    r, c = slot[0], slot[1]
    for i in range(1, 10):
        board[r][c] = i
        if isValidBoard(board):
            if solve(board):
                return True
        board[r][c] = 0
    return False


bo = readboard()
pretty(bo)
solve(bo)
pretty(bo)
